﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using GeoToolKit5;
namespace LAS_14_ToFrom_12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult di;
            di = folderBrowserDialog1.ShowDialog();
            if (di == DialogResult.OK)
            {
                textBoxInputPath.Text = folderBrowserDialog1.SelectedPath;
            }
            else
            {
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult di;
            di = folderBrowserDialog1.ShowDialog();
            if (di == DialogResult.OK)
            {
                textBox_OutputPath.Text = folderBrowserDialog1.SelectedPath;
            }
            else
            {
                return;
            }
        }
         private void button3_Click(object sender, EventArgs e)
        {
            
            DialogResult di;
            di = folderBrowserDialog1.ShowDialog();
            if (di == DialogResult.OK)
            {
                textBox_Path12op.Text = folderBrowserDialog1.SelectedPath;
            }
            else
            {
                return;
            }
        }
        private void buttonProcess_Click(object sender, EventArgs e)
        {

            if (comboBox_LAS_Option.Text == "")
            {
                MessageBox.Show("Please select option...");
                return;
            }
            if (textBoxInputPath.Text == "" || textBox_OutputPath.Text=="")
            {
                MessageBox.Show("Please select input//output folder...");
                return;
            }

            if (comboBox_LAS_Option.Text == "Update Original Las 1.4 From Converted & Edited Las 1.2" && textBox_Path12op.Text=="")
            {
                MessageBox.Show("Please select folder of LAS 1.2 file obtained after editing...");
                return;
            }
            if (comboBox_LAS_Option.Text == "Update Original Las 1.2 From Converted & Edited Las 1.2" && textBox_Path12op.Text == "")
            {
                MessageBox.Show("Please select folder of LAS 1.2 file obtained after editing...");
                return;
            }
            try
            {
                Carto5.LASFileInfo lf1 = new Carto5.LASFileInfo();
                string[] fileEntries = Directory.GetFiles(textBoxInputPath.Text, "*.las", SearchOption.AllDirectories);
                int filecnt = 0;
                if (fileEntries.Length == 0) { MessageBox.Show("No LAS file found in input folder"); return; }
                progressBar1.Minimum = 0;
                progressBar1.Maximum = fileEntries.Length;
                progressBar1.Value = 0;
                foreach (string fileName in fileEntries)
                {
                    filecnt++;
                    string[] splitedline = fileName.Split('\\');
                    string fname = splitedline[splitedline.Length - 1];
                    string[] ftype = fname.Split('.');
                    string FileExtenction = ftype[ftype.Length - 1];
                    if (FileExtenction == "LAS" || FileExtenction == "las")
                    {
                        string outputpath = textBox_OutputPath.Text;
                        //outputpath = outputpath + "\\" + fname;
                        string EditedLasfile = textBox_Path12op.Text;
                        EditedLasfile = EditedLasfile + "\\" + fname;
                        if (comboBox_LAS_Option.Text == "Convert Original Las 1.4 To Las 1.2")
                        {

                            lf1.convertLasFile(fileName, outputpath, "A");

                        }
                        else if (comboBox_LAS_Option.Text == "Update Original Las 1.4 From Converted & Edited Las 1.2")
                        {
                            lf1.deriveLasFromOrglLas14AndEditedLas12(fileName, EditedLasfile, outputpath);

                        }
                        else if (comboBox_LAS_Option.Text == "Update Original Las 1.2 From Converted & Edited Las 1.2")
                        {
                            lf1.deriveFinalLas12FromClientLas12AndEditedLas12(fileName, EditedLasfile, outputpath);
                        }
                       
                    }
                    progressBar1.Value = filecnt;
                    progressBar1.Refresh();
                    this.Refresh();
                }
                MessageBox.Show("DONE.....");
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
                return;
            }

           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox_LAS_Option.Items.Add("Convert Original Las 1.4 To Las 1.2");
            comboBox_LAS_Option.Items.Add("Update Original Las 1.4 From Converted & Edited Las 1.2");
            comboBox_LAS_Option.Items.Add("Update Original Las 1.2 From Converted & Edited Las 1.2");

           
        }

       
    }
}
